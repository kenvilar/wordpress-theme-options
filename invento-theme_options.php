<?php

add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' );

//Init plugin options to white list our options
function theme_options_init() {
	register_setting( 'invento_options', 'invento_theme_options', 'theme_options_validate' );
}

// Load up the menu page
function theme_options_add_page() {
	add_theme_page( __( 'Theme Options', 'invento' ), __( 'Theme Options', 'invento' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}

// Create arrays for our select and radio options
$select_options = array(
	'0' => array(
		'value' =>	'0',
		'label' => __( 'Zero', 'invento' )
	),
	'1' => array(
		'value' =>	'1',
		'label' => __( 'One', 'invento' )
	),
	'2' => array(
		'value' => '2',
		'label' => __( 'Two', 'invento' )
	),
	'3' => array(
		'value' => '3',
		'label' => __( 'Three', 'invento' )
	),
	'4' => array(
		'value' => '4',
		'label' => __( 'Four', 'invento' )
	),
	'5' => array(
		'value' => '3',
		'label' => __( 'Five', 'invento' )
	)
);

$radio_options = array(
	'yes' => array(
		'value' => 'yes',
		'label' => __( 'Yes', 'invento' )
	),
	'no' => array(
		'value' => 'no',
		'label' => __( 'No', 'invento' )
	),
	'maybe' => array(
		'value' => 'maybe',
		'label' => __( 'Maybe', 'invento' )
	)
);

// Create the options page
function theme_options_do_page() {
	global $select_options, $radio_options;

	if ( ! isset( $_REQUEST['settings-updated'] ) )
		$_REQUEST['settings-updated'] = false;

	?>
	<div class="wrap">
		<?php screen_icon(); echo "<h2>" . get_current_theme() . __( ' Theme Options', 'invento' ) . "</h2>"; ?>

		<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
		<div class="updated fade"><p><strong><?php _e( 'Options saved', 'invento' ); ?></strong></p></div>
		<?php endif; ?>

		<form method="post" action="options.php">
			<?php settings_fields( 'invento_options' ); ?>
			<?php $options = get_option( 'invento_theme_options' ); ?>

			<table class="form-table">

				<!-- A checkbox option -->
				<tr valign="top"><th scope="row"><?php _e( 'A checkbox', 'invento' ); ?></th>
					<td>
						<input id="invento_theme_options[invento_checkbox_option]" name="invento_theme_options[invento_checkbox_option]" type="checkbox" value="1" <?php checked( '1', $options['invento_checkbox_option'] ); ?> />
					</td>
				</tr>

				<!-- A text input option -->
				<tr valign="top"><th scope="row"><?php _e( 'Input text', 'invento' ); ?></th>
					<td>
						<input id="invento_theme_options[invento_input_text]" class="regular-text" type="text" name="invento_theme_options[invento_input_text]" value="<?php esc_attr_e( $options['invento_input_text'] ); ?>" />
					</td>
				</tr>

				<!-- A select input option -->
				<tr valign="top"><th scope="row"><?php _e( 'Select input', 'invento' ); ?></th>
					<td>
						<select name="invento_theme_options[invento_selectinput]">
							<?php
								$selected = $options['invento_selectinput'];
								$p = '';
								$r = '';

								foreach ( $select_options as $option ) {
									$label = $option['label'];
									if ( $selected == $option['value'] ) // Make default first in list
										$p = "\n\t<option style=\"padding-right: 10px;\" selected='selected' value='" . esc_attr( $option['value'] ) . "'>$label</option>";
									else
										$r .= "\n\t<option style=\"padding-right: 10px;\" value='" . esc_attr( $option['value'] ) . "'>$label</option>";
								}
								echo $p . $r;
							?>
						</select>
					</td>
				</tr>

				<!-- A radio buttons -->
				<tr valign="top"><th scope="row"><?php _e( 'Radio buttons', 'invento' ); ?></th>
					<td>
						<fieldset><legend class="screen-reader-text"><span><?php _e( 'Radio buttons', 'invento' ); ?></span></legend>
						<?php
							if ( ! isset( $checked ) )
								$checked = '';
							foreach ( $radio_options as $option ) {
								$radio_setting = $options['invento_radio_buttons'];

								if ( '' != $radio_setting ) {
									if ( $options['invento_radio_buttons'] == $option['value'] ) {
										$checked = "checked=\"checked\"";
									} else {
										$checked = '';
									}
								}
							}
						?>
						</fieldset>
					</td>
				</tr>

				<!-- A textarea option -->
				<tr valign="top"><th scope="row"><?php _e( 'A textbox', 'invento' ); ?></th>
					<td>
						<textarea id="invento_theme_options[invento_textarea]" class="large-text" cols="50" rows="10" name="invento_theme_options[invento_textarea]"><?php echo esc_textarea( $options['invento_textarea'] ); ?></textarea>
					</td>
				</tr>
			</table>

			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e( 'Save Options', 'invento' ); ?>" />
			</p>
		</form>

		<?php screen_icon(); echo "<span style='position:fixed;bottom:35px;right:20px;font-style:italic;'" . ">Theme Options from <a href='http://invento.ph/'>Invento</a></span>"; ?>

	</div>
	<?php
}

// Sanitize and validate input. Accepts an array, return a sanitized array.
function theme_options_validate( $input ) {
	global $select_options, $radio_options;

	// Our checkbox value is either 0 or 1
	if ( ! isset( $input['invento_checkbox_option'] ) )
		$input['invento_checkbox_option'] = null;
	$input['invento_checkbox_option'] = ( $input['invento_checkbox_option'] == 1 ? 1 : 0 );

	// Say our text option must be safe text  withno HTML tags
	$input['invento_input_text'] = wp_filter_nohtml_kses( $input['invento_input_text'] );

	// Our select option must actually be in our array of select options
	if ( ! array_key_exists( $input['invento_selectinput'], $select_options ) )
		$input['invento_selectinput'] = null;

	// Our radio option must actually be in our array of radio options
	if ( ! isset( $input['invento_radio_buttons'] ) )
		$input['invento_radio_buttons'] = null;
	if ( ! array_key_exists( $input['invento_radio_buttons'], $radio_options ) )
		$input['invento_radio_buttons'] = null;

	// Say our textarea option must be safe text with the allowed tags for posts
	$input['invento_textarea'] = wp_filter_post_kses( $input['invento_textarea'] );

	return $input;
}
